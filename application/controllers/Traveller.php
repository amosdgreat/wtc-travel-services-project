<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Traveller extends CI_Controller {
	function __construct()
	{
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_traveller');
    }

	public function dashboard()
	{
		if(!$this->session->userdata('traveller_id')) {
			redirect(base_url());
		} else {
			$tot = $this->Model_traveller->check_traveller($this->session->userdata('traveller_id'));
	    	if($tot) {
	    		redirect(base_url());
	    	}
		}
		$data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

		//$data['team_members'] = $this->Model_team->all_team_member();

		$this->load->view('view_header',$data);
		$this->load->view('view_traveller_dashboard',$data);
		$this->load->view('view_footer',$data);
	}

	public function payment_history()
	{
		if(!$this->session->userdata('traveller_id')) {
			redirect(base_url());
		} else {
			$tot = $this->Model_traveller->check_traveller($this->session->userdata('traveller_id'));
	    	if($tot) {
	    		redirect(base_url());
	    	}
		}
		$data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

		$data['tpayment'] = $this->Model_traveller->payment_history($this->session->userdata('traveller_id'));

		$this->load->view('view_header',$data);
		$this->load->view('view_traveller_payment_history',$data);
		$this->load->view('view_footer',$data);
	}

	public function registration() {
		$data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

		$this->load->view('view_header',$data);
		$this->load->view('view_registration',$data);
		$this->load->view('view_footer',$data);
	}

	public function registration_add()
	{		
		$data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

		$error = '';

		if(isset($_POST['form_registration'])) 
		{

			$valid = 1;

			$this->form_validation->set_rules('traveller_name', 'Traveller Name', 'trim|required');
			$this->form_validation->set_rules('traveller_email', 'Traveller Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('traveller_phone', 'Traveller Phone', 'trim|required');
			$this->form_validation->set_rules('traveller_address', 'Traveller Address', 'trim|required');
			$this->form_validation->set_rules('traveller_city', 'Traveller City', 'trim|required');
			$this->form_validation->set_rules('traveller_state', 'Traveller State', 'trim|required');
			$this->form_validation->set_rules('traveller_country', 'Traveller Country', 'trim|required');
			$this->form_validation->set_rules('traveller_password', 'Password', 'trim|required');
            $this->form_validation->set_rules('traveller_re_password', 'Retype Password', 'trim|required|matches[traveller_password]');
            $this->form_validation->set_error_delimiters('', '<br>');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }
            if($_POST['traveller_email']!='') {
	            $tot = 0;
	        	$tot = $this->Model_traveller->traveller_registration_check($_POST['traveller_email']);
	        	if($tot==1) {
	        		$error .= 'The Traveller Email Address Already Exists';
	        	}
            }
            

		    if($valid == 1)
		    {
		    	$token = md5(uniqid(rand(), true));

		    	$form_data = array(
					'traveller_name'     => $_POST['traveller_name'],
					'traveller_email'    => $_POST['traveller_email'],
					'traveller_phone'    => $_POST['traveller_phone'],
					'traveller_address'  => $_POST['traveller_address'],
					'traveller_city'     => $_POST['traveller_city'],
					'traveller_state'    => $_POST['traveller_state'],
					'traveller_country'  => $_POST['traveller_country'],
					'traveller_password' => md5($_POST['traveller_password']),
					'traveller_token'    => $token,
					'traveller_access'   => 0
	            );
	            $this->Model_traveller->registration_add($form_data);

	            $verify_link = base_url().'traveller/registration_verify/'.$_POST['traveller_email'].'/'.$token;

		    	$msg = 'Thank you for signing up! <br><br>
Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.<br><br>
Please click this link to activate your account:<br>
<a href="'.$verify_link.'">'.$verify_link.'</a>';
            	$this->load->library('email');

				$this->email->from($data['setting']['send_email_from']);
				$this->email->to($_POST['traveller_email']);

				$this->email->subject('Registration Confirmation');
				$this->email->message($msg);

				$this->email->set_mailtype("html");

				$this->email->send();

				$success = '<i class="fa fa-check"></i>&nbsp; Your registration is complete. Please check your email address to follow the process to confirm your registration. Check your spam folder also. <b>OR</b> just follow the link below to activate your account right away: <br>
<a href="'.$verify_link.'">'.$verify_link.'</a>';
        		$this->session->set_flashdata('success',$success);
		    }

		    else
		    {
		    	$form_data = array(
					'traveller_name'    => $_POST['traveller_name'],
					'traveller_email'   => $_POST['traveller_email'],
					'traveller_phone'   => $_POST['traveller_phone'],
					'traveller_address' => $_POST['traveller_address'],
					'traveller_city'    => $_POST['traveller_city'],
					'traveller_state'   => $_POST['traveller_state'],
					'traveller_country' => $_POST['traveller_country']
		        );
		        $this->session->set_flashdata('form_data', $form_data);
		    	$this->session->set_flashdata('error',$error);
		    }
		    redirect($this->agent->referrer());
		}
		else 
		{
			redirect($this->agent->referrer());
		}

		
	}

	function registration_verify($email=0,$token=0) {

		if(!$email || !$token ) {
			redirect(base_url());
		}

		$tot = $this->Model_traveller->registration_confirm_check_url($email,$token);
		if(!$tot) {
			redirect(base_url());
		}

		$data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

		$form_data = array(
			'traveller_token' => '',
			'traveller_access' => 1
        );
        $this->Model_traveller->registration_confirm_update($email,$token,$form_data);

		$this->load->view('view_header',$data);
		$this->load->view('view_thankyou',$data);
		$this->load->view('view_footer',$data);
	}

    public function login()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page'] = $this->Model_common->all_page();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['featured_packages'] = $this->Model_common->all_featured_packages();
        $data['packages'] = $this->Model_common->all_package();
        $data['all_news'] = $this->Model_common->all_news();

        $error = '';
        
        if(isset($_POST['form1'])) {

            // Getting the form data
            $traveller_email = $this->input->post('traveller_email',true);
            $traveller_password = $this->input->post('traveller_password',true);

            // Checking the email address
            $un = $this->Model_traveller->login_check_email($traveller_email);

            if(!$un) {

                $error = 'Email address is wrong!';
                $this->session->set_flashdata('error',$error);
                redirect(base_url().'traveller/login');

            } else {

                // When email found, checking the password
                $pw = $this->Model_traveller->login_check_password($traveller_email,$traveller_password);

                if(!$pw) {
                    
                    $error = 'Password is wrong!';
                    $this->session->set_flashdata('error',$error);
                	redirect(base_url().'traveller/login');

                } else {

                    // When email and password both are correct
                    $array = array(
                        'traveller_id' => $pw['traveller_id'],
                        'traveller_name' => $pw['traveller_name'],
                        'traveller_email' => $pw['traveller_email'],
                        'traveller_phone' => $pw['traveller_phone'],
                        'traveller_address' => $pw['traveller_address'],
                        'traveller_city' => $pw['traveller_city'],
                        'traveller_state' => $pw['traveller_state'],
                        'traveller_country' => $pw['traveller_country'],
                        'traveller_password' => $pw['traveller_password'],
                        'traveller_token' => $pw['traveller_token'],
                        'traveller_access' => $pw['traveller_access']
                    );
                    $this->session->set_userdata($array);

                    redirect(base_url().'traveller/dashboard');
                }
            }
        } else {
            $this->load->view('view_header',$data);
            $this->load->view('view_login',$data); 
            $this->load->view('view_footer',$data);   
        }
        
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url().'traveller/login');
    }

    function profile_update() {

    	if(!$this->session->userdata('traveller_id')) {
			redirect(base_url());
		} else {
			$tot = $this->Model_traveller->check_traveller($this->session->userdata('traveller_id'));
	    	if($tot) {
	    		redirect(base_url());
	    	}
		}

		$error = '';
		$success = '';

		$data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

		if (isset($_POST['form1'])) {

			$valid = 1;

			$this->form_validation->set_rules('traveller_name', 'Traveller Name', 'trim|required');
			$this->form_validation->set_rules('traveller_phone', 'Traveller Phone', 'trim|required');
			$this->form_validation->set_rules('traveller_address', 'Traveller Address', 'trim|required');
			$this->form_validation->set_rules('traveller_city', 'Traveller City', 'trim|required');
			$this->form_validation->set_rules('traveller_state', 'Traveller State', 'trim|required');
			$this->form_validation->set_rules('traveller_country', 'Traveller Country', 'trim|required');
            $this->form_validation->set_error_delimiters('', '<br>');

			if($this->form_validation->run() == FALSE) {
				$valid = 0;
                $error .= validation_errors();
            }

            if( !(empty($_POST['traveller_password']) && empty($_POST['traveller_re_password'])) ) {
		        if($_POST['traveller_password'] != $_POST['traveller_re_password']) {
			    	$valid = 0;
			        $error .= "Passwords do not match. Try again.";	
		    	}
		    }

		    if($valid == 1) {
		    	if( !(empty($_POST['traveller_password']) && empty($_POST['traveller_re_password'])) ) {
		    		$form_data = array(
						'traveller_name'     => $_POST['traveller_name'],
						'traveller_phone'    => $_POST['traveller_phone'],
						'traveller_address'  => $_POST['traveller_address'],
						'traveller_city'     => $_POST['traveller_city'],
						'traveller_state'    => $_POST['traveller_state'],
						'traveller_country'  => $_POST['traveller_country'],
						'traveller_password' => md5($_POST['traveller_password'])
		            );
		    	} else {
		    		$form_data = array(
						'traveller_name'     => $_POST['traveller_name'],
						'traveller_phone'    => $_POST['traveller_phone'],
						'traveller_address'  => $_POST['traveller_address'],
						'traveller_city'     => $_POST['traveller_city'],
						'traveller_state'    => $_POST['traveller_state'],
						'traveller_country'  => $_POST['traveller_country']
		            );
		    	}
		    	
	        	$this->Model_traveller->traveller_profile_update($form_data,$this->session->userdata('traveller_id'));
	        	$success = 'Information is updated successfully!';
        		$this->session->set_flashdata('success',$success);

        		$this->session->set_userdata($form_data);

        		redirect(base_url().'traveller/profile_update');
		    }
		    else {
		    	$this->session->set_flashdata('error',$error);
		    	redirect(base_url().'traveller/profile_update');
		    }


		} else {
			$this->load->view('view_header',$data);
			$this->load->view('view_traveller_profile_update',$data);
			$this->load->view('view_footer',$data);	
		}		
    }


    public function forget_password()
    {
        $data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

        $error = '';
        $success = '';

        if(isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('traveller_email', 'Email Address', 'trim|required|valid_email');

            if($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error .= validation_errors();
            } else {
                $tot = $this->Model_traveller->forget_password_check_email($_POST['traveller_email']);
                if(!$tot) {
                    $valid = 0;
                    $error .= 'You email address is not found in our system.<br>';
                }    
            }
             

            if($valid == 1) {

                $token = md5(rand());

                // Update Database
                $form_data = array(
                    'traveller_token' => $token
                );
                $this->Model_traveller->forget_password_update($_POST['traveller_email'],$form_data);
                
                // Send Email
                $msg = 'To reset your password, please <a href="'.base_url().'traveller/reset_password/'.$_POST['traveller_email'].'/'.$token.'">click here</a> and enter a new password';
                               
                $this->load->library('email');

                $this->email->from($data['setting']['send_email_from']);
                $this->email->to($_POST['traveller_email']);

                $this->email->subject('Password Reset Request');
                $this->email->message($msg);

                $this->email->set_mailtype("html");

                $this->email->send();

                $success = 'A mail has been sent to your e-mail address. Please follow the instruction in there.';
                $this->session->set_flashdata('success',$success);
                redirect(base_url().'traveller/forget_password');
            } else {
                $this->session->set_flashdata('error',$error);
		    	redirect(base_url().'traveller/forget_password');
            }
           
        } else {
        	$this->load->view('view_header',$data);
            $this->load->view('view_forget_password',$data);
            $this->load->view('view_footer',$data);
        }
        
    }


    public function reset_password($email=0,$token=0)
    {
        $tot = $this->Model_traveller->reset_password_check_url($email,$token);
        if(!$tot) {
            redirect(base_url().'traveller/login');
            exit;
        }

        $data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

        $error = '';
        $success = '';
        
        if(isset($_POST['form1'])) {
            $valid = 1;

            $this->form_validation->set_rules('traveller_password', 'Password', 'trim|required');
            $this->form_validation->set_rules('traveller_re_password', 'Retype Password', 'trim|required|matches[traveller_password]');

            if($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error = validation_errors();
            }

            $data['var1'] = $email;
            $data['var2'] = $token;

            if($valid == 1) {

                $form_data = array(
                    'traveller_password' => md5($_POST['traveller_password']),
                    'traveller_token'    => ''
                );
                $this->Model_traveller->reset_password_update($email,$form_data);
                $success = 'Password is updated successfully!';
                $this->session->set_flashdata('success',$success);
                redirect(base_url().'traveller/reset_password_success/');
            } else {
            	$this->session->set_flashdata('error',$error);
		    	redirect(base_url().'traveller/reset_password/'.$data['var1'].'/'.$data['var2']);
            }

        } else {
            $data['var1'] = $email;
            $data['var2'] = $token;
            $this->load->view('view_header',$data);
            $this->load->view('view_reset_password',$data);
            $this->load->view('view_footer',$data);
        }
        
    }

    public function reset_password_success() {
    	
    	$data['setting'] = $this->Model_common->all_setting();
		$data['page'] = $this->Model_common->all_page();
		$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['featured_packages'] = $this->Model_common->all_featured_packages();
		$data['packages'] = $this->Model_common->all_package();
		$data['all_news'] = $this->Model_common->all_news();

    	$this->load->view('view_header',$data);
        $this->load->view('view_reset_password_success',$data);
        $this->load->view('view_footer',$data);
    }
    
     // Where would you like to go To on the homepage box

    public function send_destination()
    {
    	// get user input
    	$guest_fullName = $this->input->post('guest_fullName');
		$guest_email       = $this->input->post('guest_email');
		$guest_phoneNumber = $this->input->post('guest_phoneNumber');
		// other options
		$guest_no_of_adults = $this->input->post('guest_no_of_adults');
		$guest_no_of_children = $this->input->post('guest_no_of_children');
        
        $msg = 'Hello Admin,<br> A client has just placed a trip request, Kindly see Below client\'s Details for Further Action(s) <br><br>';
		$msg.= 'Full Name: <strong>'.$guest_fullName.'</strong> <br> ';
		$msg.= 'Email Address: <strong>'.$guest_email.'</strong> <br> ';
		$msg.= 'Phone Number: <strong> '.$guest_phoneNumber.'</strong> <br> ';
		$msg.= '<h3> Additional Details</h3> <br>';
		$msg.= 'Number of Adults: '.$guest_no_of_adults.' <br> ';
		$msg.= 'Number of Children: '.$guest_no_of_children.' <br> ';
		$msg.= 'Thank you';


		if (isset($guest_fullName) && isset($guest_email) && isset($guest_phoneNumber) && isset($guest_no_of_adults) && isset($guest_no_of_children)) {
			
		}

		
		//TODO, set this in DB
		$valid_admin_list = array('ijerry@wtcaccra.com', 'eyevutsey@wtcaccra.com', 'amensah@wtcaccra.com', 'buyitgh@gmail.com');
        
        // $valid_admin_list = array('amensah@wtcaccra.com', 'buyitgh@gmail.com');

		$this->load->library('email');

		$this->email->from('info@wtctravelservices.com');
		$this->email->to($valid_admin_list);

		$this->email->subject('A New Destination Request.');
		$this->email->message($msg);

		$this->email->set_mailtype("html");

		$this->email->send();
    // 	echo "<script>alert('Yes we are here now: your Name is :  $msg ')</script>";
    	echo '<script type="text/javascript">'; 
        echo 'alert("'.$guest_fullName.': Thank you for the info, an Agent will get in touch with you shortly.");'; 
        echo 'window.location.href = "https://wtctravelservices.com";';
        echo '</script>';

    }

}