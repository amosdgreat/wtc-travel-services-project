<div class="banner-slider" style="background-image: url(<?php echo base_url(); ?>public/uploads/<?php echo $setting['banner_login']; ?>)">
	<div class="bg"></div>
	<div class="bannder-table">
		<div class="banner-text">
			<h1>Login</h1>
		</div>
	</div>
</div>

<div class="login-area bg-area pt_80 pb_80">
	<div class="container wow fadeIn">
		<div class="row">
			<div class="col-md-offset-4 col-md-4 col-md-offset-4">
				
				<?php
                if($this->session->flashdata('error')) {
                    echo '<div class="error-class">'.$this->session->flashdata('error').'</div>';
                }
                if($this->session->flashdata('success')) {
                    echo '<div class="success-class">'.$this->session->flashdata('success').'</div>';
                }
                ?>

				<div class="login-form">
					<?php echo form_open(base_url().'traveller/login',array('class' => '')); ?>
						<div class="form-row">
							<div class="form-group">
								<label for="">Email Address</label>
								<input type="text" class="form-control" name="traveller_email" placeholder="Enter your e-mail" required="">
							</div>
							<div class="form-group">
								<label for="">Password</label>
								<input type="password" class="form-control" name="traveller_password" placeholder="Enter your password" required="">
							</div>
							<button type="submit" class="btn btn-primary" name="form1"><i class="fa fa-sign-in"></i>&nbsp; Login</button> <a href="<?php echo base_url(); ?>traveller/forget_password" class="forget-password-link"><i class="fa fa-key"></i>&nbsp; forgotten your password?</a>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>