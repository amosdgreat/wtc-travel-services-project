
<?php error_reporting(0); ?>
<div class="dashboard-area bg-area pt_50 pb_80">
   <div class="container wow fadeIn">
      <div class="row">
         <div class="col-md-3 col-sm-12 wow fadeIn" data-wow-delay="0.1s">
            <div class="option-board mt_30">
               <ul>
                  <?php $this->view('view_traveller_sidebar'); ?>
               </ul>
            </div>
         </div>
         <div class="col-md-9 col-sm-12 wow fadeIn" data-wow-delay="0.2s">
            <div class="detail-dashboard mt_30">
               <h1>Hi, <?php echo $this->session->userdata('traveller_name'); ?></h1>
               <h3>Welcome to your dashboard.</h3>
               <?php 
                  $CI =& get_instance();
                  $CI->load->model('Model_traveller');
                  $d_arr = $CI->Model_traveller->check_traveller_has_package($this->session->userdata('traveller_id'));
                  
                    foreach ($d_arr as $traveler_data) {
                      $traveller_has_package = $traveler_data['traveller_has_package'];
                    }
                  ?>
               <?php
                  if ($traveller_has_package == '1'): ?>
               <div id="container">
                  <div id="success-box">
                     <div class="dot"></div>
                     <div class="dot two"></div>
                     <div class="face">
                        <div class="eye"></div>
                        <div class="eye right"></div>
                        <div class="mouth happy"></div>
                     </div>
                     <div class="shadow scale"></div>
                     <div class="message">
                        <h1 class="alert">
                           Hurray!!! <br>
                           <div id="alert_messg_show" style="text-align: center;">
                              <?php
                                 $CI =& get_instance();
                                 $CI->load->model('Model_traveller');
                                 $d_arr = $CI->Model_traveller->check_traveller_package_id($this->session->userdata('traveller_id'));
                                 foreach ($d_arr as $available_package_id) {
                                     $traveller_package_id = $available_package_id['p_id'];
                                 }
                                 // $this->Model_payment->package_name_by_package_id($p_id);
                                 $CI->load->model('Model_payment');
                                 $available_packages = $CI->Model_payment->package_name_by_package_id($traveller_package_id);
                                 // var_dump($available_packages['p_start_date']);
                                 
                                 
                                 $now = date('Y-m-d'); // or your date as well
                                 $datetime1 = new DateTime($now);
                                 
                                 $datetime2 = new DateTime($available_packages['p_start_date']);
                                 
                                 $difference = $datetime1->diff($datetime2);
                                 
                                 
                              ?>
                              
                              Your Vacation is 
                              <?php echo ' '.$difference->y.' year(s), ' 
                                 .$difference->m.' months, ' 
                                 .$difference->d.' days';  
                              ?> away.  
                           </div>
                        </h1>
                        <!-- <p>Till your next vacation</p> -->
                     </div>
                     <a href="<?php echo base_url('destination'); ?>"> <button class="button-box"><span class="green"><i class="fa fa-globe"></i>&nbsp; View more Destinations</span></button></a>
                  </div>
               </div>
               <?php else: ?>
               <div id="container">
                  <div id="error-box">
                     <div class="dot"></div>
                     <div class="dot two"></div>
                     <div class="face2">
                        <div class="eye"></div>
                        <div class="eye right"></div>
                        <div class="mouth sad"></div>
                     </div>
                     <div class="shadow move"></div>
                     <div class="message">
                        <h1 class="alert">
                           No vacation! <br>
                           <div id="alert_messg_show" style="text-align: center;">you have not selected a vacation,<br>choose available vacation package. </div>
                        </h1>
                     </div>
                     <a href="<?php echo base_url('destination'); ?>"> <button class="button-box"><span class="red"><i class="fa fa-gift"></i>&nbsp; Choose a Package</span></button></a>
                  </div>
               </div>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </div>
</div>

