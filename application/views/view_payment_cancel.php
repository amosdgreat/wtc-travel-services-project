<div class="banner-slider" style="background-image: url(<?php echo base_url(); ?>public/uploads/<?php echo $setting['banner_payment_success']; ?>)">
	<div class="bg"></div>
	<div class="bannder-table">
		<div class="banner-text">
			<h1>Payment Cancel</h1>
		</div>
	</div>
</div>

<div class="login-area bg-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="login-form" style="padding-top:50px;padding-bottom:50px;font-size:30px;color:red;text-align: center;">
					<svg class="checkmark_error" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"> <circle class="checkmark__circle_error" cx="26" cy="26" r="25" fill="none" /> <path class="checkmark__check_error" fill="none" d="M16 16 36 36 M36 16 16 36" />
					</svg> Payment is Cancelled. Try again!
				</div>
			</div>
		</div>
	</div>
</div>