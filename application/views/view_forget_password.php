<div class="banner-slider" style="background-image: url(<?php echo base_url(); ?>public/uploads/<?php echo $setting['banner_forget_password']; ?>)">
	<div class="bg"></div>
	<div class="bannder-table">
		<div class="banner-text">
			<h1>Forget Password</h1>
		</div>
	</div>
</div>

<div class="login-area bg-area pt_80 pb_80">
	<div class="container wow fadeIn">
		<div class="row">
			<div class="col-md-offset-4 col-md-4 col-md-offset-4">
				
				<?php
                if($this->session->flashdata('error')) {
                    echo '<div class="error-class">'.$this->session->flashdata('error').'</div>';
                }
                if($this->session->flashdata('success')) {
                    echo '<div class="success-class">'.$this->session->flashdata('success').'</div>';
                }
                ?>

				<div class="login-form">
					<?php echo form_open(base_url().'traveller/forget_password',array('class' => '')); ?>
						<div class="form-row">
							<div class="form-group">
								<label for="">Email Address</label>
								<input type="text" class="form-control" name="traveller_email" placeholder="Enter your e-mail address">
							</div>
							<button type="submit" class="btn btn-primary" name="form1"><i class="fa fa-send"></i>&nbsp; Submit</button> <a href="<?php echo base_url(); ?>traveller/login" class="forget-password-link"><i class="fa fa-lock"></i>&nbsp; Back to login page</a>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>