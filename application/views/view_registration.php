<div class="banner-slider" style="background-image: url(<?php echo base_url(); ?>public/uploads/<?php echo $setting['banner_registration']; ?>)">
	<div class="bg"></div>
	<div class="bannder-table">
		<div class="banner-text">
			<h1>Registration</h1>
		</div>
	</div>
</div>

<div class="register-area bg-area pt_80 pb_80">
	<div class="container wow fadeIn">
		<div class="row">
			<div class="col-md-offset-3 col-md-6 col-md-offset-3">

				<?php
                if($this->session->flashdata('error')) {
                    echo '<div class="error-class">'.$this->session->flashdata('error').'</div>';
                }
                if($this->session->flashdata('success')) {
                    echo '<div class="success-class">'.$this->session->flashdata('success').'</div>';
                }
                ?>

                <?php
                $form_data = $this->session->flashdata('form_data');
                ?>

				<div class="regiser-form sell-form">
					<?php echo form_open(base_url().'traveller/registration_add',array('class' => '')); ?>
						<div class="form-row">
							<div class="form-group">
								<label for="">Full Name *</label>
								<input type="text" class="form-control" name="traveller_name" required="" placeholder="Enter your Full Name" value="<?php echo html_escape($form_data['traveller_name']); ?>">
							</div>
							<div class="form-group">
								<label for="">Email Address *</label>
								<input type="email" class="form-control" name="traveller_email" required="" placeholder="Enter your vaild e-mail address" value="<?php echo html_escape($form_data['traveller_email']); ?>">
							</div>
							<div class="form-group">
								<label for="">Phone Number *</label>
								<input type="text" class="form-control" name="traveller_phone" required="" placeholder="Enter your phone number" value="<?php echo html_escape($form_data['traveller_phone']); ?>">
							</div>
							<div class="form-group">
								<label for="">Address *</label>
								<textarea name="traveller_address" required="" placeholder="Enter your address" class="form-control" cols="30" rows="10" style="height:120px;"><?php echo html_escape($form_data['traveller_address']); ?></textarea>
							</div>
							<div class="form-group">
								<label for="">Country *</label>
								<select name="traveller_country" class="form-control countries order-alpha" id="countryId" value="<?php echo html_escape($form_data['traveller_country']); ?>">
								    <option value="">Select Country</option>
								</select>
								<!-- <input type="text" class="form-control" name="traveller_country" value="<?php echo html_escape($form_data['traveller_country']); ?>"> -->
							</div>

							<div class="form-group">
								<label for="">State *</label>
								<select name="traveller_state" class="form-control states order-alpha" value="<?php echo html_escape($form_data['traveller_state']); ?>" id="stateId">
								    <option value="">Select State</option>
								</select>
								<!-- <input type="text" class="form-control" name="traveller_state" value="<?php echo html_escape($form_data['traveller_state']); ?>"> -->
							</div>

							<div class="form-group">
								<label for="">City *</label>
								<select name="traveller_city" class="form-control cities order-alpha" value="<?php echo html_escape($form_data['traveller_city']); ?>" id="cityId">
								    <option value="">Select City</option>
								</select>
								<!-- <input type="text" class="form-control" name="traveller_city" value="<?php echo html_escape($form_data['traveller_city']); ?>"> -->
							</div>

							<div class="form-group">
								<label for="">Password *</label>
								<input type="password" class="form-control" name="traveller_password">
							</div>
							<div class="form-group">
								<label for="">Retype Password *</label>
								<input type="password" class="form-control" name="traveller_re_password">
							</div>
							<button type="submit" class="btn btn-primary" name="form_registration"><i class="fa fa-user-plus"></i>&nbsp; Register</button>
						</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
