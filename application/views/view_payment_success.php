<div class="banner-slider" style="background-image: url(<?php echo base_url(); ?>public/uploads/<?php echo $setting['banner_payment_success']; ?>)">
	<div class="bg"></div>
	<div class="bannder-table">
		<div class="banner-text">
			<h1>Payment Request</h1>
		</div>
	</div>
</div>

<div class="login-area bg-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="login-form" style="padding-top:30px;padding-bottom:30px;font-size:30px;color:green;text-align: center;">
					<svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52"><circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/><path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/></svg>Payment request received.<br>a staff member will get in touch shortly. <br> <a href="<?php echo base_url('traveller/payment_history');?>"> <button class="btn btn-info btn-lg" type="submit" name="form_book">Check your payment status</button></a>
				</div>
			</div>
		</div>
	</div>
</div>