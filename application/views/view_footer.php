<?php if($setting['newsletter_status'] == 'Show'): ?>

<div class="newsletter-area pt_120 pb_120" style="background-image: url(<?php echo base_url(); ?>public/uploads/<?php echo $setting['newsletter_photo']; ?>)" id="newsletter">
<div class="newsletter-bg"></div>
    <div class="container wow fadeIn">
    <div class="row">
            <div class="col-md-12">
                <div class="main-headline white">
                    <div class="headline">
                        <h2><?php echo $setting['newsletter_title']; ?></h2>
                    </div>
                    <p>
                        <?php echo nl2br($setting['newsletter_text']); ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt_30 wow fadeIn" data-wow-delay="0.2s">
                <div class="newsletter-message">
                <?php
                if($this->session->flashdata('error1')) {
                    echo '<div class="error-class">'.$this->session->flashdata('error1').'</div>';
                }
                if($this->session->flashdata('success1')) {
                    echo '<div class="success-class">'.$this->session->flashdata('success1').'</div>';
                }
                ?>
                </div>
                <?php echo form_open(base_url().'newsletter/send',array('class' => '')); ?>
                    <div class="newsletter-submit">
                        <input type="text" placeholder="Enter Your Email" name="email_subscribe">
                        <input type="submit" value="Submit" name="form_subscribe">
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="footer-area pt_50 pb_80">
    <div class="container wow fadeIn">
        <div class="row">
            <div class="col-md-3 col-sm-6 wow fadeInLeft" data-wow-delay="0.2s">
                <div class="footer-item mt_30">
                    <h3>Upcoming Tours</h3>
                    <ul>
                        <?php
                        $i=0;
                        foreach ($packages as $package) {
                            $today_time = strtotime(date('Y-m-d'));
                            $start_date_time = strtotime($package['p_start_date']);
                            if($today_time >= $start_date_time) {continue;}
                            $i++;
                            if($i>$setting['total_upcoming_tour_footer']) {break;}
                            ?>
                            <li><a href="<?php echo base_url(); ?>package/view/<?php echo $package['p_id']; ?>"><?php echo $package['p_name']; ?></a></li>
                            <?php
                            
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="0.3s">
                <div class="footer-item mt_30">
                    <h3>Featured Tours</h3>
                    <ul>
                        <?php
                        $i=0;               
                        foreach ($featured_packages as $featured_package) {
                            $i++;
                            if($i>$setting['total_featured_tour_footer']) {break;}
                            ?>
                            <li><a href="<?php echo base_url(); ?>package/view/<?php echo $featured_package['p_id']; ?>"><?php echo $featured_package['p_name']; ?></a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="0.4s">
                <div class="footer-item mt_30">
                    <h3>Recent News</h3>
                    <ul>
                        <?php
                        $i=0;               
                        foreach ($all_news as $news) {
                            $i++;
                            if($i>$setting['total_recent_news_footer']) {break;}
                            ?>
                            <li><a href="<?php echo base_url(); ?>news/view/<?php echo $news['news_id']; ?>"><?php echo $news['news_title']; ?></a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeInRight" data-wow-delay="0.2s">
                <div class="footer-item mt_30">
                    <h3>Address</h3>
                    <div class="footer-address-item">
                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                        <div class="text"><span><?php echo nl2br($setting['footer_address']); ?></span></div>
                    </div>
                    <div class="footer-address-item">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <div class="text"><span><?php echo nl2br($setting['footer_phone']); ?></span></div>
                    </div>
                    <div class="footer-address-item">
                        <div class="icon"><i class="fa fa-envelope-o"></i></div>
                        <div class="text"><span><?php echo nl2br($setting['footer_email']); ?></span></div>
                    </div>
                    <ul class="footer-social">
                        <?php
                        foreach ($social as $row)
                        {
                            if($row['social_url']!='')
                            {
                                echo '<li><a href="'.$row['social_url'].'"><i class="'.$row['social_icon'].'"></i></a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="container wow fadeIn">
        <div class="row">
            <div class="col-12">
                <div class="copy-text">
                    <p><?php echo $setting['footer_copyright']; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="scroll-top">
    <i class="fa fa-angle-up"></i>
</div>


<!-- All JS Files -->

<script src="<?php echo base_url(); ?>public/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/chosen.jquery.js"></script>
<script src="<?php echo base_url(); ?>public/js/docsupport/init.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/easing.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/wow.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/superfish.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.slicknav.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/viewportchecker.js"></script>
<script src="<?php echo base_url(); ?>public/js/waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/extention/choices.js"></script>


<script>



    // function showMessage() { 
    //     var textbox = document.getElementById("dest").value;

    //     //Show message in alert() debug
    //     alert("The message is: " + textbox);
    //     console.log(textbox);
    //     // set user writtend destination
    //     document.getElementById("UserDestination").value = textbox.value;
    // }


function onClick($this) {
    var goingToDestination = $this.previousElementSibling.value;
    if (goingToDestination == '') {
        alert('Sorry a valid location is required');
        location.reload(true).die();


    } else {
        console.log(goingToDestination);
        // document.getElementById("myDiv").value = goingToDestination;

        $.post("process_destination.php", {
                UserDestination: goingToDestination,
                time: "2pm",
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            })
            .done(function(data) {
                // location.reload(9000000);
                // alert( "Data Loaded: " + data );
                // $('#amosMen').html('Hello World');
                // document.getElementById("amosMen").value = goingToDestination.value;


            });

        // $.ajax({
        //     type: 'GET',
        //     url: "get_destinations.php",
        //     async: false,
        //     success: function(text) {
        //         alert(text);
        //     }
        // });

        // document.getElementById("myDiv").innerHTML = goingToDestination;
        // $('#myDiv').html(this.goingToDestination);

        // window.document.getElementById("myDiv").innerText = "value div";

        // window.document.getElementById("myDiv").value = "value input";

        // $('#myDiv').empty().append(goingToDestination);

        // $('#myDiv').html('Hello World');

        // document.getElementById("myDiv").innerHTML += goingToDestination.innerHTML;

        // document.getElementById('myDiv').setAttribute("text",goingToDestination);

        // document.getElementsByTagName("myDiv")[0].setAttribute("type", "button");

        // document.getElementById("myDiv").setAttribute("href", "https://www.bitdegree.org");

        // $('#myDiv').attr('value','mad man');


    }
}



// function onClick(elem) {
//   var val = elem.previousElementSibling.value;;
//   //var val = $this.siblings('input[type=text]').val();
//   if (val == '') {
//     console.log('no input');
//   } else {
//     console.log(val);
//             window.document.getElementById("myDiv").value = "value input";

//   }
// }


</script>



<script src="https://js.stripe.com/v2/"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script> -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>  -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script> 

<!-- Dashboard error message for client -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

<script src="<?php echo base_url(); ?>public/js/sizzle.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/datepicker.js"></script>
<script src="<?php echo base_url(); ?>public/js/datepicker.en.js"></script>



<script src="<?php echo base_url(); ?>public/js/custom.js"></script>


<?php
$class_name = '';
$segment_2 = 0;
$segment_3 = 0;
$class_name = $this->router->fetch_class();
$segment_2 = $this->uri->segment('2');
$segment_3 = $this->uri->segment('3');
?>
<?php if($class_name == 'package'): ?>

<?php
$p_price = $this->Model_package->get_package_price_from_id($segment_2);
?>
<script>
    $(document).ready(function() {
        $('#numberPerson').on('change',function() {
            var selectVal = $('#numberPerson').val();
            var selectPrice = <?php echo $p_price['p_price_single']; ?>;
            var totalPrice = selectVal * selectPrice;
             $('#totalPrice').text(Number((totalPrice).toFixed(1)).toLocaleString());
        });
    });
</script>

<!--  slider for package budget slider -->
<!-- 
<script>
 $(function() {
    $( "#slider-range-min" ).slider({
      range: "min",
      value: 0,
      min: 0,
      step: 1,
      max: 13,
      slide: function( event, ui ) {

        $( "#amount" ).val( "$" + Number((ui.value).toFixed(1)).toLocaleString()+ " every month");

      }
    });
    $(".ui-slider-handle").text("<>");
    $( "#amount" ).val( "$" + $( "#slider-range-min" ).slider( "value") + " every month");
  });
</script> -->
 <!-- End for package budget slider -->

<?php endif; ?>

<!-- split user payment by number of months -->

<script>
    function CalculateInstallmentByMonth() {
        // var txtbox_Value = $("#totalPriceValue").val();
        var txtbox_Value = document.getElementById('totalPriceValue').getAttribute('data-value');
        var selectBox_Value = $("#Select").val();

        if (selectBox_Value == "0") {
            alert("Please select a valid Month"+ selectBox_Value);
        }
        else {
            var MultipliedValue = (txtbox_Value / selectBox_Value);
            $("#monthlyInstallmentPayment").val(MultipliedValue);
            document.getElementById("monthlyInstallmentPayment").innerHTML = "$" + Number((MultipliedValue).toFixed(1)).toLocaleString();


            // alert("Selected Month is:"+ MultipliedValue);


        }
    }
</script>

<!--  End of split user payment by number of months -->

<script>
    $(document).on('submit', '#stripe_form', function () {
        $('#submit-button').prop("disabled", true);
        $("#msg-container").hide();
        Stripe.card.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
            // name: $('.card-holder-name').val()
        }, stripeResponseHandler);
        return false;
    });
    Stripe.setPublishableKey('<?php echo $setting['stripe_public_key']; ?>');
    function stripeResponseHandler(status, response) {
        if (response.error) {
            $('#submit-button').prop("disabled", false);
            $("#msg-container").html('<div style="color: red;border: 1px solid;margin: 10px 0px;padding: 5px;"><strong>Error:</strong> ' + response.error.message + '</div>');
            $("#msg-container").show();
        } else {
            var form$ = $("#stripe_form");
            var token = response['id'];
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            form$.get(0).submit();
        }
    }
</script>
<script src="//geodata.solutions/includes/countrystatecity.js"></script>

</body>

</html>
